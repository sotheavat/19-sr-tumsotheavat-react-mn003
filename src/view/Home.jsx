import React, { useEffect, useState } from "react";
import { useDispatch, useSelector} from "react-redux";
import { deleteArticle, fetchArticle } from "../redux/action/articleAction";
import { Container, Row, Col, Card, Button } from "react-bootstrap";
import { bindActionCreators } from "redux";
import { useHistory } from "react-router";
import { fetchCategory } from "../redux/action/categoryAction";
import ReactLoading from 'react-loading';
import ReactPaginate from 'react-paginate';
import { strings } from "../localization/localize";

export default function Home() {
    const dispatch = useDispatch();
    const onDelete = bindActionCreators(deleteArticle, dispatch);
    const article = useSelector((states) => states.articleReducer);
    const category = useSelector((states) => states.categoryReducer.categories);
    const history = useHistory();
    const [page, setPage] = useState(1)

    const [isArticleLoading, setIsArticleLoading] = useState(true)
    const [isCategoryLoading, setIsCategoryLoading] = useState(true)

    useEffect(() => {
        dispatch(fetchArticle(page)).then(()=>{
            setIsArticleLoading(false)
        });
        dispatch(fetchCategory()).then(()=>{
            setIsCategoryLoading(false)
        });
    }, [dispatch,page]);

    const Loading = () => (
        <ReactLoading className='loading'  type={"spin"} height={250} width={150} color={"black"} />
    );

    const onPageChange= ({selected})=>{
        dispatch(fetchArticle(selected+1))
    }

    return (
        <Container className="my-4">
            {isArticleLoading && isCategoryLoading ? Loading() :
                <Row>
                    <Col >
                        <h1>{strings.Category}</h1>
                        <Row className="my-3">
                            {category.map((item, index) => (
                                <Button key={index} className="mx-1" variant="outline-secondary" size="sm">{item.name}</Button>
                            ))}
                        </Row>
                        <Row className="my-3">
                            {article.articles.map((item, index) => (
                                <Col key={index} className="py-2" md="4">
                                    <Card>
                                        <Card.Img variant="top" style={{ objectFit: "cover", height: "150px" }} src={item.image} />
                                        <Card.Body>
                                            <Card.Title>{item.title}</Card.Title>
                                            <Card.Text className="text-line-3">{item.description}</Card.Text>
                                            <Button variant="primary" size='sm' onClick={()=>history.push('/view/'+item._id)}>{strings.Read}</Button>{" "}
                                            <Button
                                                variant="warning"
                                                size='sm'
                                                onClick={()=>{
                                                    history.push('/update/article/'+item._id)
                                                }}
                                            >{strings.Edit}</Button>{" "}
                                            <Button variant="danger" size='sm' onClick={()=> onDelete(item._id)}>{strings.Delete}</Button>
                                        </Card.Body>
                                    </Card>
                                </Col>
                            ))}
                        </Row>
                        <ReactPaginate
                            pageCount={article.totalPage}
                            onPageChange={onPageChange}
                            containerClassName="pagination pagination-sm justify-content-center"
                            pageClassName="page-item"
                            pageLinkClassName="page-link"
                            previousClassName="page-item"
                            previousLinkClassName="page-link"
                            nextLinkClassName="page-link"
                            nextClassName="page-item"
                            activeClassName="active"
                        />
                    </Col>
                </Row>
            }
        </Container>
    );
}
