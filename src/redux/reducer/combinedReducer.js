import { combineReducers } from 'redux';
import articleReducer from './articleReducer';
import authorReducer from './authorReducer';
import categoryReducer from './categoryReducer';

const reducers = combineReducers({
    articleReducer,
    authorReducer,
    categoryReducer
});

export default reducers;