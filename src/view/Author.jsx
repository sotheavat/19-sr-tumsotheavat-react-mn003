import React, {useEffect, useRef, useState} from "react";
import { useDispatch, useSelector } from "react-redux";
import { deleteAuthor, fetchAuthor, postAuthor, updateAuthor } from "../redux/action/authorAction";
import { Container, Form, Row, Col, Button, Table } from "react-bootstrap";
import { bindActionCreators } from "redux";
import { uploadImage } from "../redux/action/articleAction";
import sl from 'sweetalert2'
import { strings } from "../localization/localize";


export default function Author() {
  const [imageURL, setImageURL] = useState(
    "https://designshack.net/wp-content/uploads/placeholder-image.png"
  );
  const [imageFile, setImageFile] = useState(null);
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [isUpdate, setIsUpdate] = useState(null);
  const [isRefresh, setIsRefresh] = useState(false);
  const isFirstRun = useRef(true);
  const [nameError, setNameError] = useState("");
  const [emailError, setEmailError] = useState("");
  const dispatch = useDispatch();
  const onDelete = bindActionCreators(deleteAuthor, dispatch);
  const onUploadImg = bindActionCreators(uploadImage, dispatch);
  const onPost = bindActionCreators(postAuthor, dispatch);
  const onUpdateAuthor= bindActionCreators(updateAuthor, dispatch);
  const state = useSelector((states) => states.authorReducer.authors);

  useEffect(() => {
    if (!isFirstRun.current) {
      let pattern3 = /[\w\s]{4,}/g;
      let result3 = pattern3.test(name.trim());
      if (name.trim() === "") {
        setNameError("Author name Cannot be blank!");
      } else if (!result3) {
        setNameError(
            "Author name Cannot contain any special character and at lease 4 character !"
        );
      } else {
        setNameError("");
      }
    }
  }, [name]);
  useEffect(() => {
    if (!isFirstRun.current) {
      let pattern =
          /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/g;
      let result = pattern.test(email.trim());
      if (result) {
        setEmailError("");
      } else if (email.trim() === "") {
        setEmailError("Email cannot be empty!");
      } else {
        setEmailError("Email is invalid!");
      }
    }
    isFirstRun.current = false;
  }, [email]);


  useEffect(() => {
    dispatch(fetchAuthor());
    setIsRefresh(false)
  }, [isRefresh,dispatch]);

  const onAdd = async (e) => {
    e.preventDefault();
    let newAuthor = { name, email };

    if (imageFile) {
      let url = await onUploadImg(imageFile);
      newAuthor.image = url.payload;
    }
    onPost(newAuthor).then((message) => {
        setIsRefresh(true)
        sl.fire({
            position: 'top',
            icon: 'success',
            title: message.payload,
            showConfirmButton: false,
            timer: 1500
          })
    });
    setImageFile(null);
    setName('');
    setEmail('');
    clear();
  };

  function clear(){
    document.getElementById("name").value = "";
    document.getElementById("email").value = "";
    document.getElementById("img").value = "";
    document.getElementById("image").src =
      "https://designshack.net/wp-content/uploads/placeholder-image.png";
  }

  const onEdit = (id, tempName, tempEmail, image) => {
    setIsUpdate(id)
    setName(tempName)
    setEmail(tempEmail)
    document.getElementById("name").value = tempName;
    document.getElementById("email").value = tempEmail;
    document.getElementById("image").src = image;
  };

  const onUpdate = async (e) => {
    e.preventDefault();
    let newAuthor = { name, email };

    if (imageFile) {
        let url = await onUploadImg(imageFile);
        newAuthor.image = url.payload;
      }

    onUpdateAuthor(isUpdate, newAuthor).then((message) => {
        sl.fire({
            position: 'top',
            icon: 'success',
            title: message.payload,
            showConfirmButton: false,
            timer: 2000
          })
    });
    setIsRefresh(true)
    setIsUpdate(null);
    setImageFile(null);
    setName('');
    setEmail('');
    clear();
  };

  return (
    <div>
      <Container>
        <h1>{strings.Author}</h1>
        <Row>
          <Col md={8}>
            <Form>
              <Form.Group>
                <Form.Label>{strings.Author}</Form.Label>
                <Form.Control
                  id="name"
                  type="text"
                  placeholder="Author Name"
                  onChange={(e) => setName(e.target.value)}
                />
                <Form.Text className="text-danger">{nameError}</Form.Text>
              </Form.Group>

              <Form.Group>
                <Form.Label>{strings.Email}</Form.Label>
                <Form.Control
                  id="email"
                  type="text"
                  placeholder="Email"
                  onChange={(e) => setEmail(e.target.value)}
                ></Form.Control>
                <Form.Text className="text-danger">{emailError}</Form.Text>
              </Form.Group>
              <Button
                variant="primary"
                type="submit"
                onClick={isUpdate !== null ? onUpdate : onAdd}
              >
                {isUpdate !== null ? strings.Save : strings.Add}
              </Button>
            </Form>
          </Col>
          <Col md={4}>
            <img id="image" className="w-75" alt="..." src={imageURL} />
            <Form>
              <Form.Group>
                <Form.File
                  id="img"
                  label={strings.ChooseImage}
                  onChange={(e) => {
                    let url = URL.createObjectURL(e.target.files[0]);
                    setImageFile(e.target.files[0]);
                    setImageURL(url);
                  }}
                />
              </Form.Group>
            </Form>
          </Col>
        </Row>

        <Table striped bordered hover>
          <thead>
            <tr>
              <th>ID</th>
              <th>{strings.Name}</th>
              <th>{strings.Email}</th>
              <th>{strings.Img}</th>
              <th>{strings.Action}</th>
            </tr>
          </thead>
          <tbody>
            {state.map((item, index) => (
              <tr key={index}>
                <td>{item._id}</td>
                <td>{item.name}</td>
                <td>{item.email}</td>
                <td>
                  <img src={item.image} alt="" width="auto" height={100} />
                </td>
                <td>
                  <Button
                    size="sm"
                    variant="warning"
                    onClick={() =>
                      onEdit(item._id, item.name, item.email, item.image)
                    }
                  >
                    {strings.Edit}
                  </Button>{" "}
                  <Button
                    size="sm"
                    variant="danger"
                    onClick={() => onDelete(item._id)}
                  >
                    {strings.Delete}
                  </Button>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
      </Container>
    </div>
  );
}